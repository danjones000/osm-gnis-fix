#!/bin/bash

set -eu

declare -r osmChange=$1
declare -r out=`mktemp`

# Only works on macOs currently

curl "$osmChange" > "$out"

(
    echo -n 'node["gnis:feature_id"](id:'
    xq -r '.osmChange.create[].node["@id"]' < "$out" | tr '\n' , | sed 's/,$//'
    echo ');'
    echo 'out;'
) | pbcopy

pbpaste
echo
echo "Saving xml to $out"
