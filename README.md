# OSM GNIS fix

While contributing to [OpenStreetMap][] recently, I came across a point of interest that I knew was misplaced. It was a few hundred meters away from where it was supposed to be.

After some digging, I found that this point was added in 2009, along with a lot of other data, imported from [GNIS][] data.

It turns out that there are many points in this data that are misplaced. Some of these have been corrected, but many of them have not.

My solution was to create a [MapRoulette][] challenge, so that people can more easily correct this data, bit by bit.

This repository includes a shell script I use that converts the changesets from OSM into a query that can be used to create new challenges on [MapRoulette][].

I'm creating a new challenge for each changeset from this import that I can find, and grouping all the challenges under a [single project](https://maproulette.org/browse/projects/47895).

I will also, in this document, keep track of the challenges I've created, and what I'm working on with this project.

[OpenStreetMap]: https://www.openstreetmap.org/
[GNIS]: https://www.usgs.gov/us-board-on-geographic-names
[MapRoulette]: https://maproulette.org/

## Process for adding new challenges

I start with a particular changeset on OSM, [e.g.](https://www.openstreetmap.org/changeset/748530).

To find a changeset to do, I start with the [most recent][], and then near the bottom, go to the previous one.

![previous changeset](images/previous.png).

At the bottom of the page is a link to "osmChange XML". I copy this URL.

![osmChange XML Link](images/xml-link.png)

Next, I run the script here, providing that link as a single argument.

This script will create an Overpass query that grabs all the nodes from this changeset, and still has a `gnis:feature_id` tag. This is likely the ways that haven't yet been fixed, as these should generally be converted into areas, and the data moved to the area.

The script will copy this Overpass query to the clipboard.

Next, in MapRoulette, I clone the most recent challenge. 

!["Clone Challenge" in MapRoulette](images/clone-challenge.png)

I create a name that's similar to the previous challenge, but with the type of POIs that I saw in the changeset. Then, I paste the Overpass query into the "Location of your task data" field.

Finally, I customize the Changeset Info for the challenge, and put the challenge title in there.

Eventually, I get to the first from this set of imports, by going backwards. When I do, I'll start back at the [first][] one, and then start going forward.

## Changesets

I'm currently working my way back from the first one I found

- [First changeset I found][first]
- [Most recent][most recent]

### Completed so far with challenges

- https://www.openstreetmap.org/changeset/751242 - https://maproulette.org/browse/challenges/26973
- https://www.openstreetmap.org/changeset/749606 - https://maproulette.org/browse/challenges/26974
- https://www.openstreetmap.org/changeset/748530 - https://maproulette.org/browse/challenges/26975

[first]: https://www.openstreetmap.org/changeset/751242
[most recent]: https://www.openstreetmap.org/changeset/748530
